# Communicate with arduino

import serial
import time

class SensorController:
	def __init__(self):
		self.ser = serial.Serial('/dev/ttyACM0', 9600, timeout=1)
		self.ser.flush()
		self.readSensorSignal = b'\x02'
		self.togglePumpSignal = b'\x01'  
		
	def request_sensor_reading(self):
		self.write_signal(self.readSensorSignal)
		time.sleep(0.1) #give the arduino some time to answer
		lines = ""
		while self.ser.in_waiting > 0:
			lines = lines + self.ser.readline().decode('utf-8').rstrip()
		
		return lines
		
	def toggle_pump(self):
		self.write_signal(self.togglePumpSignal)
		
	def write_signal(self, signal):
		self.ser.write(signal)