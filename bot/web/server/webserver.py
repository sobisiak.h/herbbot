import datetime
import os
from flask import Flask, render_template
from bot.web.views.index import IndexView
from bot.web.views.settings import SettingsView

class HerbBotWebServer:
	def __init__(self):
		self.app = Flask(__name__, static_folder='../../../static', template_folder='../../../templates')
		
		self.indexView = IndexView(app=self.app)
		self.settingsView = SettingsView(app=self.app) 		
		
		self.app.secret_key = os.urandom(12)
		self.app.run(debug=True, port=8088, host='0.0.0.0', ssl_context='adhoc')
	
	def shut_down(self):
		self.indexView.stop_tasks()