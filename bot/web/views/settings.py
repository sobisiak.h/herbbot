from flask import Flask, render_template, request
from bot.settings.settings_manager import SettingsManager

class SettingsView:
	def __init__(self, app):
		self.app = app
		self.app.add_url_rule('/settings', view_func=self.get_settings)
		self.app.add_url_rule('/settings/save', view_func=self.save_settings, methods=["POST"])
		self.settings = SettingsManager()
		
	def get_settings(self):
		self.load_settings()
		
		templateData = {
      	'dryValue': self.settings.dryValue,
      	'pumpTime' : self.settings.pumpTimePerMl,
      	'readInterval' : self.settings.readSensorInterval,
      	'waterAmountInMl' : self.settings.waterAmountInMl,
      	'imageInterval' : self.settings.takeImageInterval,
      	'takeImage' : self.convert_to_string(self.settings.takeImage),
      	'readSensor' : self.convert_to_string(self.settings.readSensor),
      	'executeWaterSchedule' : self.convert_to_string(self.settings.executeWaterSchedule),
      	'url' : self.settings.automationUrl,
      	'user' : self.settings.automationUser,
      	'lightId' : self.settings.lightId,
      	'fanId' : self.settings.fanId,
      	'runLight' : self.convert_to_string(self.settings.runLight),
      	'runFan' : self.convert_to_string(self.settings.runFan)
      }
		response = self.app.make_response(render_template('settings.html', **templateData))
		return response
		
	def load_settings(self):	
		self.settings.load_settings()
		
	def save_settings(self):
		dryValue = request.form['dryValue']
		waterAmountInMl = request.form['waterAmountInMl']
		pumpTime = request.form['pumpTime']
		readInterval = request.form['readInterval']
		imageInterval = request.form['imageInterval']
		takeImage = request.form['takeImage']
		readSensor = request.form['readSensor']
		executeWaterSchedule = request.form['executeWaterSchedule']
		url = request.form['url']
		user = request.form['user']
		lightId = request.form['lightId']
		fanId = request.form['fanId']
		runLight = request.form['runLight']
		runFan = request.form['runFan']
		
		try: 
			self.settings.pumpTimePerMl = float(pumpTime)
		except:
			print("Pumping time must be between 0.01 and 10")
		
		try:	
			self.settings.dryValue = int(dryValue)
		except:
			print("Must be a value between 0 and 9999")
		
		try:
			self.settings.waterAmountInMl = int(waterAmountInMl)
		except:
			print("Must be int")
		
		try:
			self.settings.readSensorInterval = float(readInterval)
		except:
			print("Must be float")			
		
		try:
			self.settings.takeImageInterval = float(imageInterval)
		except:
			print("Must be float")		
		
		self.settings.executeWaterSchedule = self.convert_to_bool(executeWaterSchedule)
		self.settings.readSensor = self.convert_to_bool(readSensor)
		self.settings.takeImage = self.convert_to_bool(takeImage)
		self.settings.runLight = self.convert_to_bool(runLight)
		self.settings.runFan = self.convert_to_bool(runFan)
		self.settings.automationUrl = url
		self.settings.automationUser = user
		self.settings.lightId = lightId
		self.settings.fanId = fanId
		
		self.settings.save_settings()		
		return "settings saved "
		
	def convert_to_bool(self, val):
		if(val == "on"):
			return True
		else:
			return False
			
	def convert_to_string(self, val):
		if(val is True):
			return "checked"
		else:
			return ""