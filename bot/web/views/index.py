import os
import glob
import datetime
from flask import Flask, render_template, flash, redirect, request, session, abort
from bot.controller.controller import HerbBotController
from bot.db.user_management import HerbBotUserManagement

class IndexView:
	def __init__(self, app):
		self.herbBotController = HerbBotController()
		self.app = app
		self.app.add_url_rule('/', view_func=self.get_view)
		self.app.add_url_rule('/image', view_func=self.get_image)
		self.app.add_url_rule('/<device>/<action>', view_func=self.start_tasks)
		self.app.add_url_rule('/reading', view_func=self.get_reading)
		self.app.add_url_rule('/login', view_func=self.do_login, methods=["POST"])
		self.userManagement = HerbBotUserManagement()
		
	def get_view(self):
		if not session.get('logged_in'):
			return render_template('login.html')
		else:
			response = self.app.make_response(render_template('index.html'))
			return response
	
	def do_login(self):
		if self.userManagement.check_user(request.form['username'], request.form['password']):
			session['logged_in'] = True
		return self.get_view()
	
	def get_image(self):
		list_of_files = glob.glob('./static/*.jpg')
		latest_file = max(list_of_files, key=os.path.getctime)
		return latest_file
		
	def get_reading(self):
		data = "no data"
		with open(self.herbBotController.logFile) as file:
			lines = ""
			for line in (file.readlines() [-10:]):
				lines = line + ", " + lines
			data = "[" + lines[:-2] + "]"
		return data
	
	def stop_tasks(self):
		self.herbBotController.stop_tasks()	
	
	def start_tasks(self, device, action):
		if(device == "reading"):
			if(action == "start"):
				self.herbBotController.start_tasks()
				response = "reading started"
			
			if(action == "stop"):
				self.herbBotController.stop_tasks()
				response = "reading stopped"
		
		if(device == "pump"):
			self.herbBotController.water_plant()
			response = "Watering plant"
			
		if(device == "light"):
			if(action == "on"):
				self.herbBotController.light_on()
				response = "Light on"
			elif(action == "off"):
				self.herbBotController.light_off()
				response = "Light off"
				
		if(device == "fan"):
			if(action == "on"):
				self.herbBotController.fan_on()
				response = "Fan on"
			elif(action == "off"):
				self.herbBotController.fan_off()
				response = "Fan off"
		
		return response
		