import datetime
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from bot.db.user_model import User
from werkzeug.security import generate_password_hash, check_password_hash



class HerbBotUserManagement:
	def __init__(self):
		self.engine = create_engine('sqlite:///herbbot.db', echo=True)

		# create a Session
		self.Session = sessionmaker(bind=self.engine)
		self.session = self.Session()

		pass
		
	def check_user(self, username, password):
		query = self.session.query(User).filter(User.username.in_([username]))
		user = query.first()
				
		if user is not None and check_password_hash(user.password, password):
			return True
		else:
			return False
		
	def create_first_user(self):
		user = User("herbbot","admin")
		self.session.add(user)

		# commit the record the database
		self.session.commit()