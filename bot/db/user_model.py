from sqlalchemy import create_engine, ForeignKey, Column
from sqlalchemy import Column, Date, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref
from werkzeug.security import generate_password_hash, check_password_hash

engine = create_engine('sqlite:///herbbot.db', echo=True)
Base = declarative_base()


class User(Base):

	__tablename__ = "users"

	id = Column(Integer, primary_key=True)
	username = Column(String)
	password = Column(String)


	def __init__(self, username, password):
		self.username = username
		self.password = generate_password_hash(password)

# create tables
Base.metadata.create_all(engine)