import requests
from bot.settings.settings_manager import SettingsManager

class HerbBotAutomation:
	StateOn = {'on': True}
	StateOff = {'on': False}
	
	def __init__(self):
		self.settings = SettingsManager()
		self.settings.load_settings()
		self.ApiUrl = self.settings.automationUrl + "/api"
		
	def switch_lights_on(self):
		self.do_request(self.settings.lightId, self.StateOn)
		
	def switch_lights_off(self):
		self.do_request(self.settings.lightId, self.StateOff)		
		
	def switch_fan_on(self):
		self.do_request(self.settings.fanId, self.StateOn)
		
	def switch_fan_off(self):
		self.do_request(self.settings.fanId, self.StateOff)		
		
	def do_request(self, device, state):
		url = self.ApiUrl + "/" + self.settings.automationUser + "/lights/" + device + "/state/"
		response = requests.put(url, json = state)
		print(response.text)