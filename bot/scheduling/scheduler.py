import schedule
import time
import threading
from bot.scheduling.schedule_plan import SchedulePlan

class Scheduler:
	def __init__(self):
		self.schedule = schedule
		self.schedulePlan = SchedulePlan()
		self.schedulePlan.load_schedule()		
		
	def init_water_schedule(self, job):
		# for every day set up schedule
		self.set_job_list(self.schedule.every().monday, self.schedulePlan.get_watering_time_plan(self.schedulePlan.MONDAY), job)
		self.set_job_list(self.schedule.every().tuesday, self.schedulePlan.get_watering_time_plan(self.schedulePlan.TUESDAY), job)
		self.set_job_list(self.schedule.every().wednesday, self.schedulePlan.get_watering_time_plan(self.schedulePlan.WEDNESDAY), job)
		self.set_job_list(self.schedule.every().thursday, self.schedulePlan.get_watering_time_plan(self.schedulePlan.THURSDAY), job)
		self.set_job_list(self.schedule.every().friday, self.schedulePlan.get_watering_time_plan(self.schedulePlan.FRIDAY), job)
		self.set_job_list(self.schedule.every().saturday, self.schedulePlan.get_watering_time_plan(self.schedulePlan.SATURDAY), job)
		self.set_job_list(self.schedule.every().sunday, self.schedulePlan.get_watering_time_plan(self.schedulePlan.SUNDAY), job)
		
	def init_light_schedule(self, on_job, off_job):
		self.init_device_schedule(self.schedulePlan.LIGHT_PLAN, on_job, off_job)
		
	def init_fan_schedule(self, on_job, off_job):
		self.init_device_schedule(self.schedulePlan.FAN_PLAN, on_job, off_job)		
		
	def init_device_schedule(self, device, on_job, off_job):
		self.set_job(self.schedule.every().monday, self.schedulePlan.get_on_state_time(device, self.schedulePlan.MONDAY), on_job)
		self.set_job(self.schedule.every().tuesday, self.schedulePlan.get_on_state_time(device, self.schedulePlan.TUESDAY), on_job)
		self.set_job(self.schedule.every().wednesday, self.schedulePlan.get_on_state_time(device, self.schedulePlan.WEDNESDAY), on_job)
		self.set_job(self.schedule.every().thursday, self.schedulePlan.get_on_state_time(device, self.schedulePlan.THURSDAY), on_job)
		self.set_job(self.schedule.every().friday, self.schedulePlan.get_on_state_time(device, self.schedulePlan.FRIDAY), on_job)
		self.set_job(self.schedule.every().saturday, self.schedulePlan.get_on_state_time(device, self.schedulePlan.SATURDAY), on_job)
		self.set_job(self.schedule.every().sunday, self.schedulePlan.get_on_state_time(device, self.schedulePlan.SUNDAY), on_job)
		
		self.set_job(self.schedule.every().monday, self.schedulePlan.get_off_state_time(device, self.schedulePlan.MONDAY), off_job)
		self.set_job(self.schedule.every().tuesday, self.schedulePlan.get_off_state_time(device, self.schedulePlan.TUESDAY), off_job)
		self.set_job(self.schedule.every().wednesday, self.schedulePlan.get_off_state_time(device, self.schedulePlan.WEDNESDAY), off_job)
		self.set_job(self.schedule.every().thursday, self.schedulePlan.get_off_state_time(device, self.schedulePlan.THURSDAY), off_job)
		self.set_job(self.schedule.every().friday, self.schedulePlan.get_off_state_time(device, self.schedulePlan.FRIDAY), off_job)
		self.set_job(self.schedule.every().saturday, self.schedulePlan.get_off_state_time(device, self.schedulePlan.SATURDAY), off_job)
		self.set_job(self.schedule.every().sunday, self.schedulePlan.get_off_state_time(device, self.schedulePlan.SUNDAY), off_job)

	def set_job_list(self, scheduler_day, plan, job):
		for time in plan:
			scheduler_day.at(time).do(job)

	def set_job(self, scheduler_day, time, job):
		scheduler_day.at(time).do(job)

	def start_job_task(self):
		#do this with a task
		self.scheduler_thread = threading.Thread(target = self.check_scheduler_task)
		self.scheduler_thread.start()
			
	def stop_job_task(self):
		if self.scheduler_thread is not None:
			self.scheduler_thread.do_run = False
		
	def check_scheduler_task(self):
		while getattr(self.scheduler_thread, "do_run", True):
			self.schedule.run_pending()
			time.sleep(1)
			