#Load, Save and provide the watering schedule

import yaml
import os

class SchedulePlan:
	WATERING_PLAN = "Water"
	LIGHT_PLAN = "Light"
	FAN_PLAN = "Fan"
	MONDAY = "Monday"
	TUESDAY = "Tuesday"
	WEDNESDAY = "Wednesday"
	THURSDAY = "Thursday"
	FRIDAY = "Friday"
	SATURDAY = "Saturday"
	SUNDAY = "Sunday"
	
	ON_STATE = "OnState"
	OFF_STATE = "OffState"
	
	def __init__(self):
		self.scheduleFileName = "schedule.yaml"
		
		self.scheduleData = {
				self.WATERING_PLAN: {
					self.MONDAY : ["10:00", "22:00"],
					self.TUESDAY : ["10:00", "22:00"],
					self.WEDNESDAY : ["10:00", "22:00"],
					self.THURSDAY : ["10:00", "22:00"],
					self.FRIDAY : ["10:00", "22:00"],
					self.SATURDAY : ["10:00", "22:00"],
					self.SUNDAY : ["10:00", "22:00"]
				},
				self.LIGHT_PLAN: {
					self.MONDAY : {
						self.ON_STATE : "16:00",
						self.OFF_STATE : "8:00"},
					self.TUESDAY : {
						self.ON_STATE : "16:00",
						self.OFF_STATE : "8:00"},
					self.WEDNESDAY : {
						self.ON_STATE : "16:00",
						self.OFF_STATE : "8:00"},
					self.THURSDAY : {
						self.ON_STATE : "16:00",
						self.OFF_STATE : "8:00"},
					self.FRIDAY : {
						self.ON_STATE : "16:00",
						self.OFF_STATE : "8:00"},
					self.SATURDAY : {
						self.ON_STATE : "16:00",
						self.OFF_STATE : "8:00"},
					self.SUNDAY : {
						self.ON_STATE : "16:00",
						self.OFF_STATE : "8:00"}
				},
				self.FAN_PLAN: {
					self.MONDAY : {
						self.ON_STATE : "16:00",
						self.OFF_STATE : "8:00"},
					self.TUESDAY : {
						self.ON_STATE : "16:00",
						self.OFF_STATE : "8:00"},
					self.WEDNESDAY : {
						self.ON_STATE : "16:00",
						self.OFF_STATE : "8:00"},
					self.THURSDAY : {
						self.ON_STATE : "16:00",
						self.OFF_STATE : "8:00"},
					self.FRIDAY : {
						self.ON_STATE : "16:00",
						self.OFF_STATE : "8:00"},
					self.SATURDAY : {
						self.ON_STATE : "16:00",
						self.OFF_STATE : "8:00"},
					self.SUNDAY : {
						self.ON_STATE : "16:00",
						self.OFF_STATE : "8:00"}
				}
			}

	def get_watering_time_plan(self, day):
		return self.scheduleData[self.WATERING_PLAN][day]
	
	def get_on_state_time(self, device, day):
		return self.scheduleData[device][day][self.ON_STATE]
		
	def get_off_state_time(self, device, day):
		return self.scheduleData[device][day][self.OFF_STATE]
		
	def load_schedule(self):
		if os.path.exists(self.scheduleFileName):
			with open(self.scheduleFileName, "r") as yamlfile:
				self.scheduleData = yaml.load(yamlfile, Loader=yaml.FullLoader)
				yamlfile.close()
		else:
			self.save_schedule()
		
	def save_schedule(self):
		if os.path.exists(self.scheduleFileName):
			os.remove(self.scheduleFileName)
		with open(self.scheduleFileName, 'w') as yamlfile:
			yaml.dump(self.scheduleData, yamlfile)
			yamlfile.close()