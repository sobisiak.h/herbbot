# Controls the sensor reading and watering

import time
import threading
import logging
from logging.handlers import RotatingFileHandler
from bot.settings.settings_manager import SettingsManager
from bot.sensoring.sensor_controller import SensorController
from bot.camera.camera import Camera
from bot.scheduling.scheduler import Scheduler
from bot.automation.automation import HerbBotAutomation

class HerbBotController:
	def __init__(self):
		self.settings = SettingsManager()
		self.settings.load_settings()
		self.automation = HerbBotAutomation()
		self.camera = Camera()		
		
		self.init_log()
		self.init_schedules()		
		self.start_tasks()
		
	def init_log(self):
		self.logFormatter = logging.Formatter('{"time": "%(asctime)s", "value": %(message)s}')
		self.logFile = 'sensor.log'

		self.logHandler = RotatingFileHandler(self.logFile, mode='a', maxBytes=1024*1024, 
                                 backupCount=5, encoding=None, delay=0)
		self.logHandler.setFormatter(self.logFormatter)
		self.logHandler.setLevel(logging.INFO)

		self.appLog = logging.getLogger('root')
		self.appLog.setLevel(logging.INFO)
		self.appLog.addHandler(self.logHandler)
		
	def init_schedules(self):
		self.scheduler = Scheduler()
		if(self.settings.executeWaterSchedule is True):
			self.scheduler.init_water_schedule(job=self.water_plant)
			
		if(self.settings.runLight is True):
			self.scheduler.init_light_schedule(on_job=self.light_on, off_job=self.light_off)
		
		if(self.settings.runFan is True):
			self.scheduler.init_fan_schedule(on_job=self.fan_on, off_job=self.fan_off)
		
	#starting a task for periodic reading
	def start_tasks(self):		
		if(self.settings.readSensor is True):
			self.controller = SensorController()
			self.reader_thread = threading.Thread(target = self.read_sensor)
			self.reader_thread.start()
		
		if(self.settings.takeImage is True):
			self.image_thread = threading.Thread(target = self.take_image)
			self.image_thread.start()

		self.scheduler.start_job_task()
		
	#stopping the task in order to not leave unstopped threads behind
	def stop_tasks(self):
		
		self.scheduler.stop_job_task()
		try:
			if self.reader_thread is not None:
				self.reader_thread.do_run = False
		except AttributeError:
			print("reader thread not running")	
		
		try:
			if self.image_thread is not None:
				self.image_thread.do_run = False
		except AttributeError:
			print("image thread not running")
		
	def read_sensor(self):
		while getattr(self.reader_thread, "do_run", True):
			data = self.controller.request_sensor_reading()
			self.appLog.info('"' + data + '"')
			time.sleep(self.settings.readSensorInterval)
			
	def take_image(self):
		while getattr(self.image_thread, "do_run", True):
			imageFile = self.camera.take_picture()
			
			time.sleep(self.settings.takeImageInterval)
		
	#water the plant
	def water_plant(self):
		self.lastWateringTime = time.time()
		self.appLog.info("Watering plant started")
		self.controller.toggle_pump()
		time.sleep(self.wateringTime)
		self.controller.toggle_pump()
		self.appLog.info("Watering plant stopped")
		
	def light_on(self):
		self.appLog.info("Lights on")
		self.automation.switch_lights_on()
		
	def light_off(self):
		self.appLog.info("Lights off")
		self.automation.switch_lights_off()
		
	def fan_on(self):
		self.appLog.info("Fan on")
		self.automation.switch_fan_on()
		
	def fan_off(self):
		self.appLog.info("Fan off")
		self.automation.switch_fan_off()