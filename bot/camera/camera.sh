#!/bin/bash

FILE_NAME=$1

fswebcam -r 1920x1080 -S 20 --no-banner $FILE_NAME
mv $FILE_NAME static/$FILE_NAME
