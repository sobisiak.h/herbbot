import os
import datetime
import sys
import time
import subprocess

class Camera:
	def __init__(self):
		self.script_dir = os.path.dirname(__file__)
		
	def take_picture(self):
		imgFileName = datetime.datetime.now().strftime("%Y-%m-%d_%H%M%S") +".jpg"
		os.system('./camera.sh ' + imgFileName)
		
		rel_path = os.path.join("static", imgFileName)
		#abs_file_path = os.path.join(self.script_dir, rel_path)
		return rel_path