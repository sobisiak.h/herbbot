# Reading, writing and providing properties 

import yaml
import os

class SettingsManager:
	def __init__(self):
		self.dryValue = 900
		self.waterAmountInMl = 100
		self.pumpTimePerMl = 0.05 #time needed to pump one mililiter
		self.readSensorInterval = 5
		self.takeImageInterval = 60
		self.takeImage = True
		self.readSensor = True
		self.executeWaterSchedule = True
		self.settingsFileName = "settings.yaml"
		self.automationUrl = ""
		self.automationUser = ""
		self.lightId = ""
		self.fanId = ""
		self.runLight = False
		self.runFan = False

	def load_settings(self):
		if os.path.exists(self.settingsFileName):
			with open(self.settingsFileName, "r") as yamlfile:
				data = yaml.load(yamlfile, Loader=yaml.FullLoader)
				self.dryValue = data['Main']['dryValue']
				self.waterAmountInMl = data['Main']['waterAmountInMl']
				self.pumpTimePerMl = data['Main']['pumpTimePerMl']
				self.readSensorInterval = data['Main']['readSensorInterval']
				self.takeImageInterval = data['Main']['takeImageInterval']
				self.takeImage = data['Main']['takeImage']
				self.executeWaterSchedule = data['Main']['executeWaterSchedule']
				self.readSensor = data['Main']['readSensor']
				self.automationUrl = data['Automation']['url']
				self.automationUser = data['Automation']['user']
				self.lightId = data['Automation']['lightId']
				self.fanId = data['Automation']['fanId']
				self.runLight = data['Automation']['runLight']
				self.runFan = data['Automation']['runFan']
				yamlfile.close()    	

	def save_settings(self):
		settings_data = {
				'Main': {
					'dryValue' : self.dryValue,
					'waterAmountInMl': self.waterAmountInMl,
					'pumpTimePerMl' : self.pumpTimePerMl,
					'readSensorInterval' : self.readSensorInterval,
					'takeImageInterval' : self.takeImageInterval,
					'takeImage' : self.takeImage,
					'readSensor' : self.readSensor,
					'executeWaterSchedule' : self.executeWaterSchedule
					},
				'Automation': {
					'url' : self.automationUrl,
					'user' : self.automationUser,
					'lightId' : self.lightId,
					'fanId' : self.fanId,
					'runLight' : self.runLight,
					'runFan' : self.runFan
				}
			}

		if os.path.exists(self.settingsFileName):
			os.remove(self.settingsFileName)
		with open(self.settingsFileName, 'w') as yamlfile:
			yaml.dump(settings_data, yamlfile)
			yamlfile.close()
		

        