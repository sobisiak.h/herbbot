from bot.web.server.webserver import HerbBotWebServer

def main():
	herbBotServer = None
	try:
		print("HerbBot 1.0")
		herbBotServer = HerbBotWebServer()
	finally:
		herbBotServer.shut_down()

if __name__ == "__main__":
	main()
