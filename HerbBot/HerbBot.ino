//the relays connect to
int IN1 = 3;
int IN2 = 4;
int IN3 = 5;
int IN4 = 6;

int PROBE = 1;

int probeDryValue = 540;

int pumpingTimeInMs = 3000;
int readIntervalInMs = 1000;

int currentTime = 0;
int lastReadTime = 0;
int pumpStartTime = 0;

int incomingByte = 0; // for incoming serial data

bool isPumpRunning = false;

#define ON   0
#define OFF  1
void setup() 
{
  Serial.begin(9600);
  relay_init();//initialize the relay
}

void loop() {
  communicate_with_server();
}

void communicate_with_server() {
  //send_sensor_data();
  
  // send data only when you receive data:
  if (Serial.available() > 0) {
    // read the incoming byte:
    incomingByte = Serial.read();

    // say what you got:
    //Serial.print("I received: ");
    //Serial.println(incomingByte, DEC);
    
    if(incomingByte == 1) {
      //Serial.println("it is one!");
      if(isPumpRunning) {
        stop_pump();
      }
      else {
        run_pump();
      }
    } else if(incomingByte == 2) {
      send_sensor_data();
    }
  }
}

void send_sensor_data() {
  int probeValue;
  /*
  currentTime = millis();

  if(getTimeFromCurrent(lastReadTime) > readIntervalInMs) {
    probeValue = read_probe();
    Serial.println(probeValue);
    lastReadTime = millis();  
  }
  */
  probeValue = read_probe();
  Serial.println(probeValue);
}


void run_pump_on_value() {
  int probeValue;
  currentTime = millis();

  if(getTimeFromCurrent(lastReadTime) > readIntervalInMs) {
    probeValue = read_probe();
    lastReadTime = millis();  
  }
  
  if(!isPumpRunning && probeValue >= probeDryValue) {
      run_pump();
  }

  if(isPumpRunning && getTimeFromCurrent(pumpStartTime) > pumpingTimeInMs) {
    stop_pump();
  }
}

int getTimeFromCurrent(int val) {
  return currentTime - val;
}

void run_pump() {
  Serial.println("Starting pump");
  pumpStartTime = millis();
  isPumpRunning = true;
  relay_SetStatus(OFF, OFF, ON, OFF);//turn on RELAY_1
}

void stop_pump() {
  relay_SetStatus(OFF, OFF, OFF, OFF);
  isPumpRunning = false;
  Serial.println("Stopping pump");
}

int read_probe() {
  int val = analogRead(PROBE);
  return val;
}

void relay_run() {
  relay_SetStatus(ON, OFF, OFF,OFF);//turn on RELAY_1 
  Serial.println("1");
  delay(2000);//delay 2s
  relay_SetStatus(OFF, ON, OFF,OFF);//turn on RELAY_2
  Serial.println("2");
  delay(2000);//delay 2s
  relay_SetStatus(OFF, OFF, ON,OFF);//turn on RELAY_3
  Serial.println("3");
  delay(2000);//delay 2s
  relay_SetStatus(OFF, OFF, OFF,ON);//turn on RELAY_3
  Serial.println("4");
  delay(2000);//delay 2s
}

void relay_init(void)//initialize the relay
{  
  //set all the relays OUTPUT
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT);
  relay_SetStatus(OFF,OFF,OFF,OFF);//turn off all the relay
}
//set the status of relays
void relay_SetStatus( unsigned char status_1,  unsigned char status_2, unsigned char status_3,unsigned char status_4)
{
  digitalWrite(IN1, status_1);
  digitalWrite(IN2, status_2);
  digitalWrite(IN3, status_3);
  digitalWrite(IN4, status_4);

}
