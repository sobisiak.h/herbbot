function doRequest(path, targetElement, isImage) {
				console.log( "requesting..." );
				$.get(path, function(data, status){
					if (status == "success") {
						if(isImage) {
							document.getElementById(targetElement).setAttribute('src', data);
						} else {
							var jsonData = JSON.parse(data);
							var s = "<table><tr><td>Time</td><td>Sensor value</td></tr>";
							for (var i = 0; i < jsonData.length; i++){
								var obj = jsonData[i];
								s += "<tr>";
								for (var key in obj){
									var attrName = key;
									s += "<td>" + obj[key] + "</td>";
								}
								s += "</tr>";
							}
							s += "</table>";
							document.getElementById(targetElement).innerHTML = s;
						}
					}					
				});
			}
			
			function startReading() {
				doRequest('/reading/start', 'sensorData', false);
			}
			
			function stopReading() {
				doRequest('/reading/stop', 'sensorData', false);
				
			}
			
			function waterPlant() {
				doRequest('/pump/start', 'sensorData', false);
			}
			
			function lightOn() {
				doRequest('/light/on', 'sensorData', false);
			}
			function lightOff() {
				doRequest('/light/off', 'sensorData', false);
			}
			function fanOn() {
				doRequest('/fan/on', 'sensorData', false);
			}
			function fanOff() {
				doRequest('/fan/off', 'sensorData', false);
			}
			
			function polling() {
				doRequest('/reading', 'sensorData', false);
				doRequest('/image', 'liveImage', true);
			}
			
			$(document).ready(function(){
				polling(); //initial call
				setInterval(polling, 10000);
			});