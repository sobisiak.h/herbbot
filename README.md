# HerbBot


## What is HerbBot?

HerbBot is an open source software to monitor plants and automate watering and environment control. HerbBot runs in python and is currently made for linux systems (windows will follow).

## Features
- Live images over webcam
- Sensor reading and scheduled watering using Arduino

## Installing HerbBot

### Harware needed

Web front end and live imaging:
- Raspberry Pi or PC running Ubuntu (or any linux) with a webcam

For sensoring:
- [Arduino board](https://www.arduino.cc/)
- [Arduino irrigation kit](https://www.amazon.com/-/de/dp/B09BMVS366/ref=sr_1_2?keywords=arduino+irrigation+kit&qid=1671980737&sprefix=arduino+iri%2Caps%2C176&sr=8-2)

For light and fan control:
- [Phoscon ConBee II USB ZigBee GateWay](https://phoscon.de/en/conbee2)
- Smart plugs: Any that supports ZigBee protocol will do. The goal is to not be dependent on a third party cloud service. [ZigBee](https://en.wikipedia.org/wiki/Zigbee) is an open wireless communication standard for IoT devices and doesn't require any connection to a closed cloud. 

### Software

- Raspian installed on Raspberry Pi or any linux environment on a computer
- Python 3
- Pip 3
- fswebcam
- HerbBot scripts
- Arduino IDE for flashing scripts
- Arduino sketch for watering/sensor reading

Plan for wiring Arduino with soil moisture sensor and water pump is about to come!

### Running HerbBot

- clone the repo
- install all software needed
- install all dependencies with ```pip3 install -r requirements.txt```
- make camera script executeable ```chmod +x camera.sh```
- run HerbBot ```python3 herbbot.py```
- open in browser https://your_raspberry_pi_ip:8080

## License

This project is under [GNU General Public License v3.0](https://www.gnu.org/licenses/gpl-3.0.html)